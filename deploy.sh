#!/bin/bash
# Takes care of deploying the traceroute program to an SSH server.
#
# Command format:
# deploy.sh <user>@<host>
#
# Example: deploy.sh john@example.com
#


# Set environment variables if not already set.

# Build the program with gradle
gradle installDist

# Create application directory if it not already exists
ssh "$1" "mkdir -p collector"

# Copy the program with SCP
scp -r build/install/TraceCollector/* "$1":~/collector

# Launch the application
ssh "$1" "~/collector/bin/TraceCollector &>~/collector/collector.log &"

