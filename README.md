# Introduction

TraceCollector is a project that contains a collector program that can run traceroutes on demand.
By connecting with TCP on a configurable port (default: 8000) we can send requests in JSON format to do traceroutes.
Multiple traceroutes can be done at the same time.

# How to start the Trace Collector.
There are two ways to start the trace collector.
Either by using the deployment script (deploy.sh) which is contained at the root of the project.
Or by using the Dockerfile and docker-compose.yml that contain a default configuration.

## Using the deployment script.
To use the deployment script you need to SSH access to the server that you want to deploy to.
To deploy you can use the following command.

```
deploy.sh <user>@<host>
```
An example would be:
```
deploy.sh cveenman@192.42.130.21
```

Note: You need to set the environment on the host yourself. The program does not take any default values at the moment.

## Using docker
If you want to use the docker command you can start it using the following command:
```
docker-compose up
```

## Redeployment
If you redeploy on a server that already contains a running instance,
the new application will automatically stop the old one and take over.
Notice that connections will be reset.

# Configuring the trace collector
To configure the trace collector you need to set certain environment variables.

```
COLLECTOR_PORT: The port that we want to listen to for incoming requests.
```
```
COLLECTOR_MAX_CONCURRENT: The maximum amount of traceroutes that will be executed concurrently
```
```
COLLECTOR_FIRST_HOP: You can set the first hop to a larger value then 1, in order to remove the strain on the predictable first hops.
```