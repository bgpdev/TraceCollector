package collector;

import com.google.gson.JsonObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;

/**
 * The TracerouteReader object can read Traceroute results and output them as JsonObjects.
 */
public class TracerouteReader
{
    /** Used to read lines from the inputstream. */
    private BufferedReader reader;

    /**
     * Construct a TracerouteReader.
     * @param stream The InputStream of which traceroute results are read.
     * @throws IOException Any exceptions that occur during construction.
     */
    public TracerouteReader(InputStream stream) throws IOException
    {
        this.reader = new BufferedReader(new InputStreamReader(stream));

        // Read and print out the banner.
        System.out.println(reader.readLine());
    }

    /**
     * Read the results of a traceroute command.
     * @return A JsonObject with the parsed results.
     * @throws IOException Any exceptions that occur during parsing.
     */
    public JsonObject read() throws IOException
    {
        // Read a line.
        String line = reader.readLine();

        if(line == null)
            return null;

        String[] words = line.trim().split("\\s+");

        JsonObject trace = new JsonObject();
        trace.addProperty("hop", words[0]);

        if(words[1].equals("*"))
        {
            trace.addProperty("rtt",  "*");
        }
        else
        {
            trace.addProperty("responder",  words[1]);
            trace.addProperty("rtt",  words[2]);
        }

        trace.addProperty("timestamp", new Date().getTime());

        return trace;
    }
}
