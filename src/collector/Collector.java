package collector;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.*;
import java.lang.management.ManagementFactory;
import java.net.Socket;
import java.util.Date;
import java.util.Scanner;

/**
 * The main class that processes requests and runs traceroutes.
 */
public class Collector extends JsonServer
{
    /**
     * Constructs a collector.
     */
    public Collector()
    {
        super(Configuration.getConfig().port, Configuration.getConfig().max_concurrent);
    }

    /**
     * Whenever a message from a certain connection is received this message will be called.
     * @param socket The socket on which the message was received.
     * @param request The JsonObject that represents the request being made.
     */
    @Override
    protected void onMessage(Socket socket, JsonObject request)
    {
        try
        {
            if (request.has("destination"))
            {
                // Do a traceroute in a separate process.
                String address = request.get("destination").getAsString();
                String[] commands = {"traceroute",
                        "-F",
                        "--first=" + Configuration.getConfig().first_hop,
                        "--max-hops=20",
                        "--sim-queries=20",
                        "--queries=1",
                        "-n",
                        address};
                Process process = Runtime.getRuntime().exec(commands);

                // Construct the response.
                JsonObject response = new JsonObject();
                response.addProperty("destination", address);
                response.add("traces", new JsonArray());

                // Check if any error message is present.
                if (process.getErrorStream().available() > 0)
                    new BufferedReader(new InputStreamReader(process.getErrorStream())).lines().forEach(System.out::println);
                else
                {
                    TracerouteReader reader = new TracerouteReader(process.getInputStream());

                    JsonObject trace = reader.read();
                    while (trace != null)
                    {
                        System.out.println(trace.toString());
                        response.get("traces").getAsJsonArray().add(trace);
                        trace = reader.read();
                    }

                    socket.getOutputStream().write(response.toString().getBytes());
                }
            }
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
        }
    }

    /**
     * The main entry of the program.
     * @param argv Currently unused.
     * @throws Exception Any exceptions that occur while running the program.
     */
    public static void main(String[] argv) throws Exception
    {
        // Setup the global configuration from environment variables.
        Configuration.getConfig();

        /* -----------------------------------
         * Reading and creating a new PID file
         * -----------------------------------*/
        String pid = ManagementFactory.getRuntimeMXBean().getName().split("@")[0];
        System.out.println("[" + new Date().toString() + "] PID: " + pid);

        File file = new File("collector.pid");
        System.out.println("PID File location: " + file.getAbsolutePath());
        if(file.exists())
        {
            System.out.println("Shutting down the previously running server...");
            Runtime.getRuntime().exec("kill " + new Scanner(file).nextInt());
            Thread.sleep(5000);
        }

        // Create a new PID file since the old one should be non-existent or deleted.
        if(!file.createNewFile())
        {
            System.out.println("Error: Could not create PIDFile.");
            return;
        }

        FileWriter writer = new FileWriter(file, false);
        writer.write(pid);
        writer.flush();

        // If the program exists delete the pid file.
        file.deleteOnExit();

        /* -----------------------------------
         * Create a new collector.
         * -----------------------------------*/
        System.out.println("[" + new Date().toString() + "] Starting collector...");
        Collector collector = new Collector();
        collector.open();
    }
}
