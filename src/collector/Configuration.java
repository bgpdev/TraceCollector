package collector;

/**
 * Configuration class that holds all global variables.
 */
class Configuration
{
    /** The Singleton configuration class. */
    private static Configuration configuration = null;

    /** The port on which the server will listen for incoming requests. */
    final int port;

    /** The maximum amount of concurrent traceroutes that may be done. */
    final int max_concurrent;

    /** The first hop of where a traceroute should start. */
    final int first_hop;

    /**
     * Constructs a Configuration object.
     */
    private Configuration()
    {
        /* --------------------------------------------
         * Verify if the environment variables are set
         * --------------------------------------------*/
        port = System.getenv("COLLECTOR_PORT") != null ? Integer.parseInt(System.getenv("COLLECTOR_PORT")) : 8000;
        max_concurrent = System.getenv("COLLECTOR_MAX_CONCURRENT") != null ? Integer.parseInt(System.getenv("COLLECTOR_MAX_CONCURRENT")) : 10;
        first_hop = System.getenv("COLLECTOR_FIRST_HOP") != null ? Integer.parseInt(System.getenv("COLLECTOR_FIRST_HOP")) : 2;

        System.out.println("COLLECTOR_PORT: " + port);
        System.out.println("COLLECTOR_MAX_CONCURRENT: " + max_concurrent);
        System.out.println("COLLECTOR_FIRST_HOP: " + first_hop);
    }

    /**
     * Retrieves the global Configuration object.
     * @return The global Configuration object.
     */
    static Configuration getConfig()
    {
        if(configuration == null)
            configuration = new Configuration();
        return configuration;
    }
}
