package collector;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public abstract class JsonServer
{
    /** The server socket on which will be listened. */
    private ServerSocket socket;

    /** The Threadpool which will be used to accept incoming connections. */
    private ThreadPoolExecutor service;

    /**
     * Constructs a JsonServer that listens to json requests.
     * @param port The port on which the JsonServer should listen.
     * @param handlers The amount of threads that should be created to do traceroutes.
     */
    JsonServer(int port, int handlers)
    {
        try
        {
            System.out.println("Constructing a listening socket...");
            socket = new ServerSocket(port);
            System.out.println("Done. Listening on port " + port);
        }
        catch (IOException e)
        {
            e.printStackTrace();
            System.exit(-1);
        }

        // The ExecutorService (Threadpool) which allows us to do requests in parallel.
        System.out.println("[" + new Date().toString() + "] Creating Threadpool ...");
        System.out.println("Handlers: " + 5);
        service = new ThreadPoolExecutor(handlers,
                handlers,
                Long.MAX_VALUE,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(handlers));
        service.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
    }

    /**
     * Start listening to incoming requests.
     */
    public void open()
    {
        while(true)
        {
            try
            {
                System.out.println("Listening to incoming connections...");
                Socket client = socket.accept();
                System.out.println("Accepted!: " + client.getInetAddress());
                service.submit(() -> process(client));
            }
            catch (IOException e)
            {
                e.printStackTrace();
                System.exit(-1);
            }
        }
    }

    /**
     * Wait for input on a socket.
     * @param socket The socket from which input should be read.
     */
    private void process(Socket socket)
    {
        try
        {
            JsonReader reader = new JsonReader(new InputStreamReader(socket.getInputStream()));
            JsonParser parser = new JsonParser();
            while(!socket.isClosed())
            {
                JsonElement element = parser.parse(reader);
                System.out.println("Received: " + element.toString());
                if (element instanceof JsonObject)
                    service.submit(() -> onMessage(socket, (JsonObject) element));
            }
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
            try
            {
                socket.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    /**
     * The handler which will be called when a JsonObject has been received.
     * @param socket The socket from which the JsonObject has been received.
     * @param request The JsonObject that has been received.
     */
    protected abstract void onMessage(Socket socket, JsonObject request);

}
