package collector;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.Date;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * The Connector class can be used to connect to a Trace Collector.
 */
public class Connector
{
    /** The socket which represents the connection to the Trace Collector. */
    private Socket socket;

    /** The pending requests. */
    private LinkedBlockingQueue<PendingRequest> pending;

    /**
     * A PendingRequest object represents a traceroute that is currently being done on
     * the external Trace Collector server.
     */
    private class PendingRequest
    {
        String destination;
        CompletableFuture<JsonObject> future;
    }

    /**
     * Connect to a host:port endpoint.
     */
    public void connect(String host, int port, int buffer) throws IOException
    {
        // Construct a connection with the Trace Collector.
        socket = new Socket(host, port);
        pending = new LinkedBlockingQueue<>(buffer);

        new Thread(() ->
        {
            try
            {
                JsonReader reader = new JsonReader(new InputStreamReader(socket.getInputStream()));
                JsonParser parser = new JsonParser();
                while(!socket.isClosed())
                {
                    JsonElement element = parser.parse(reader);
                    if (element instanceof JsonObject)
                    {
                        JsonObject object = (JsonObject)element;
                        for(PendingRequest request : pending)
                        {
                            if(object.get("destination").getAsString().equals(request.destination))
                            {
                                pending.remove(request);
                                request.future.complete(object);
                                break;
                            }
                        }
                    }
                    else
                        throw new Exception("Element is not of type JsonObject.");
                }
            }
            catch(Exception exception)
            {
                exception.printStackTrace();
            }
        }).start();
    }

    /**
     * Creates an SSH Port forward such that services behind an SSH connection can be detected.
     * @param user The user that we want to login with on the remote server.
     * @param host The host that we want to portforward to.
     * @param local The local port to forward.
     * @param remote The remote port to forward to.
     * @param key The key that we want to connect with.
     * @param password The password that is used to unlock the key.
     * @throws Exception Any exceptions that occur while trying to do an SSH port forward.
     */
    public void setSSHPortForward(String user, String host, int local, int remote, String key, String password) throws Exception
    {
        JSch jsch = new JSch();
        jsch.addIdentity(key, password);
        Session session = jsch.getSession(user, host, 22);
        session.setPortForwardingL(local, host, remote);
        session.setConfig("StrictHostKeyChecking", "no");
        session.connect();

        System.out.println("[" + new Date().toString() + "] Created an SSH port forward from " + local + ":" + host + ":" + remote);
    }

    /**
     * Creates an SSH Port forward such that services behind an SSH connection can be detected.
     * @param user The user that we want to login with on the remote server.
     * @param host The host that we want to portforward to.
     * @param local The local port to forward.
     * @param remote The remote port to forward to.
     * @param key The key that we want to connect with.
     * @throws Exception
     */
    public void setSSHPortForward(String user, String host, int local, int remote, String key) throws Exception
    {
        JSch jsch = new JSch();
        jsch.addIdentity(key);
        Session session = jsch.getSession(user, host, 22);
        session.setPortForwardingL(local, host, remote);
        session.setConfig("StrictHostKeyChecking", "no");
        session.connect();

        System.out.println("[" + new Date().toString() + "] Created an SSH port forward from " + local + ":" + host + ":" + remote);
    }

    /**
     * Request a traceroute to a specific IPv4 or IPv6 address.
     * @param address The IPv4 address on which a traceroute should be performed.
     */
    public synchronized Future<JsonObject> request(String address) throws Exception
    {
        PendingRequest request = new PendingRequest();
        request.destination = address;
        request.future = new CompletableFuture<>();
        pending.put(request);

        // Construct the Request
        JsonObject x = new JsonObject();
        x.addProperty("destination", address);

        // Send the request to the Trace Collector.
        socket.getOutputStream().write(x.toString().getBytes());

        return request.future;
    }
}
