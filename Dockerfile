#
# This Dockerfile is used to build the final production image of the Trace Collector.
#

FROM gradle:alpine AS gradle

# Set the working directory to /srv/collector
WORKDIR /srv/collector/

# Set the current user of the image to 'root'
USER root

# Copy over the project to the Dockerfile
COPY . /srv/collector/

# Create a java executable by running installDist
RUN gradle installDist

#
# The final stage of the Multistage Docker image.
# By using multistage builds we wipe everything from before clean.
#

FROM openjdk:8-jre-slim-stretch

# Set the working directory again.
WORKDIR /srv/collector/

# Install traceroute
RUN apt-get update && apt-get install traceroute -y

# Copy over the resources and the binary executable.
COPY --from=gradle /srv/collector/build/install/collector/ /srv/collector/

# Run the Trace Collector program.
CMD ["/srv/collector/bin/collector"]
